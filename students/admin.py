# -*- coding: utf-8 -*-

from django.contrib import admin
from students.models import Student, Group, Journals, Exam
from django.core.urlresolvers import reverse
from django.forms import ModelForm, ValidationError

# Register your models here.


class StudentFormAdmin(ModelForm):

    def clean_students_group(self):
        groups = Group.objects.filter(leader=self.instance)
        if len(groups) > 0 and self.cleaned_data['students_group'] != groups[0]:
            raise ValidationError(u'Студент є старостою іншої групи', code='invalid')

        return self.cleaned_data['students_group']


class JournalAdmin(admin.ModelAdmin):
    list_display = ('student', 'format_date',)

    def format_date(self, obj):
        return obj.date.strftime('%b, %Y')

    format_date.admin_order_field = 'data'
    format_date.short_description = 'Date'


class GroupFormAdmin(ModelForm):

    def clean_leader(self):
        students_in_group = Student.objects.filter(students_group=self.instance)
        if self.cleaned_data['leader'] not in students_in_group:
            raise ValidationError(u'Студент не є членом цієї групи', code='invalid')

        return self.cleaned_data['leader']


class StudentAdmin(admin.ModelAdmin):
    list_display = ['last_name', 'first_name', 'ticket', 'students_group']
    list_display_links = ['last_name', 'first_name']
    ordering = ['last_name']
    list_filter = ['students_group']
    list_editable = ['students_group']
    list_per_page = 10
    search_fields = ['last_name', 'first_name', 'ticket', 'notes']
    form = StudentFormAdmin

    def get_view_on_site_url(self, obj=None):
        return reverse('students_edit', kwargs={'pk': obj.id})


class GroupAdmin(admin.ModelAdmin):
    list_display = ['title', 'leader']
    form = GroupFormAdmin


admin.site.register(Student, StudentAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Journals, JournalAdmin)
admin.site.register(Exam)
