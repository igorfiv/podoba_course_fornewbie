$('#div_id_birthday input').datepicker({
    format: "mm/dd/yyyy",
    todayBtn: "linked",
    language: "ru",
    daysOfWeekHighlighted: "0,6",
    todayHighlight: true
});