# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models

# Create your models here.


class Student(models.Model):

    # Student model

    class Meta(object):
        verbose_name = u"Студент"
        verbose_name_plural = u"Студенти"

    first_name = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Ім’я"
    )

    last_name = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Прізвище"
    )

    middle_name = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"По-батькові"
    )

    birthday = models.DateField(
        blank=False,
        verbose_name=u"Дата народження",
        null=True
    )

    photo = models.ImageField(
        blank=True,
        verbose_name=u"Фото",
        null=True
    )

    ticket = models.CharField(
        max_length=256,
        verbose_name=u"Білет",
        blank=False
    )

    notes = models.TextField(
        blank=True,
        verbose_name=u"Додаткові нотатки"
    )

    students_group = models.ForeignKey('Group',
                                       verbose_name=u"Група",
                                       blank=True,
                                       null=True,
                                       on_delete=models.PROTECT)

    def __unicode__(self):
        return u"%s %s" % (self.first_name, self.last_name)


class Journals(models.Model):

    class Meta(object):
        verbose_name = u'Журнал'
        verbose_name_plural = u'Журнали'

    student = models.ForeignKey('Student',
                                verbose_name=u'Студент',
                                blank=False,
                                null=True,
                                on_delete=models.SET_NULL)

    date = models.DateField(
        verbose_name=u'Дата',
        blank=False,
        null=True)

    # def __init__(self):
    #     super(Journals, self).__init__(self)
    #
    #     for i in range(1, 31):
    #         setattr(self, 'day' + u'%s' % i, False)

    # import ipdb; ipdb.set_trace()

    day1 = models.BooleanField(default=False)
    day2 = models.BooleanField(default=False)
    day3 = models.BooleanField(default=False)
    day4 = models.BooleanField(default=False)
    day5 = models.BooleanField(default=False)
    day6 = models.BooleanField(default=False)
    day7 = models.BooleanField(default=False)
    day8 = models.BooleanField(default=False)
    day9 = models.BooleanField(default=False)
    day10 = models.BooleanField(default=False)
    day11 = models.BooleanField(default=False)
    day12 = models.BooleanField(default=False)
    day13 = models.BooleanField(default=False)
    day14 = models.BooleanField(default=False)
    day15 = models.BooleanField(default=False)
    day16 = models.BooleanField(default=False)
    day17 = models.BooleanField(default=False)
    day18 = models.BooleanField(default=False)
    day19 = models.BooleanField(default=False)
    day20 = models.BooleanField(default=False)
    day21 = models.BooleanField(default=False)
    day22 = models.BooleanField(default=False)
    day23 = models.BooleanField(default=False)
    day24 = models.BooleanField(default=False)
    day25 = models.BooleanField(default=False)
    day26 = models.BooleanField(default=False)
    day27 = models.BooleanField(default=False)
    day28 = models.BooleanField(default=False)
    day29 = models.BooleanField(default=False)
    day30 = models.BooleanField(default=False)
    day31 = models.BooleanField(default=False)

    def __unicode__(self):
        return u"%s %s" % (self.student.first_name, self.student.last_name)


class Group(models.Model):

    class Meta(object):
        verbose_name = u"Група"
        verbose_name_plural = u"Групи"

    title = models.CharField(
        max_length=256,
        blank=False,
        verbose_name=u"Назва")

    leader = models.OneToOneField(
        'Student',
        verbose_name=u"Староста",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='leader_profile')

    notes = models.TextField(
        blank=True,
        verbose_name=u"Додаткові нотатки")

    def __unicode__(self):
        if self.leader:
            return u"%s (%s %s)" % (self.title, self.leader.first_name, self.leader.last_name)
        else:
            return u"{}".format(self.title)


class Exam(models.Model):

    class Meta(object):
        verbose_name = u'Екзамен'
        verbose_name_plural = u'Екзамени'

    name = models.CharField(
        verbose_name=u'Назва іспиту',
        max_length=256,
        null=False,
        blank=False
    )

    data = models.DateTimeField(
        verbose_name=u'Дата іспиту',
        null=False,
        blank=True
    )

    mentor_name = models.CharField(
        max_length=256,
        verbose_name=u'ПІБ викладача',
        null=False,
        blank=True
    )

    group = models.ForeignKey('Group',
                              verbose_name=u'Група',
                              null=True,
                              blank=False)

    def __unicode__(self):
        return "%s %s" % (self.name, self.mentor_name)
