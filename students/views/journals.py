# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render
from ..models import Student, Group, Journals
import datetime, calendar
from datetime import date, datetime
from dateutil.relativedelta import *
from calendar import *
from django.core.urlresolvers import reverse
from django import views


class JournalsView(views.generic.TemplateView):
    template_name = 'journals/journals_view.html'

    def get_context_data(self, **kwargs):
        # get context data from TemplateView class
        context = super(JournalsView, self).get_context_data(**kwargs)
        
        # перевіряємо чи передали нам місяць в параметрі,
        # якщо ні - вичисляємо поточний;
        # поки що ми віддаємо лише поточний:
        if self.request.GET.get('month'):
            month = datetime.strptime(self.request.GET.get('month'), '%Y-%m-%d').date()
        else:
            today = datetime.today()
            month = date(today.year, today.month, 1)

        # обчислюємо поточний рік, попередній і наступний місяці
        # а поки прибиваємо їх статично:
        next_month = month + relativedelta(month=1)
        prev_month = month - relativedelta(month=1)

        context['prev_month'] = prev_month.strftime('%Y-%m-%d')
        context['next_month'] = next_month.strftime('%Y-%m-%d')
        context['year'] = month.year

        # також поточний місяць;
        # змінну cur_month ми використовуватимемо пізніше
        # в пагінації; а month_verbose в
        # навігації помісячній:
        context['cur_month'] = month.strftime('%Y-%m-%d')
        context['month_verbose'] = month.strftime('%B')

        # тут будемо обчислювати список днів у місяці,
        # а поки заб'ємо статично:
        myear, mmonth = month.year, month.month
        number_of_days = monthrange(myear, mmonth)[1]
        context['month_header'] = [
            {'day': d, 'verbose': day_abbr[weekday(myear, mmonth, d)][:2]} for d in range(1, number_of_days + 1)]

        # витягуємо усіх студентів посортованих по
        queryset = Student.objects.order_by('last_name')

        # це адреса для посту AJAX запиту, як бачите, ми
        # робитимемо його на цю ж в'юшку; в'юшка журналу
        # буде і показувати журнал і обслуговувати запити
        # типу пост на оновлення журналу;
        update_url = reverse('journal_list')

        # пробігаємось по усіх студентах і збираємо
        # необхідні дані:
        students = []
        for student in queryset:

            # TODO: витягуємо журнал для студента і
            # вибраного місяця
            # набиваємо дні для студента
            try:
                journal = Journals.objects.get(student=student, date=month)
            except Exception:
                journal = None
            days = []
            for day in range(1, 31):
                days.append({
                'day': day,
                'present': journal or getattr(journal, 'day%d' % day, False) or False,
                'date': date(myear, mmonth, day).strftime('%Y-%m-%d'),
                })

            # набиваємо усі решту даних студента
            students.append({
                'fullname': u'%s %s' % (student.last_name, student.first_name),
                'days': days,
                'id': student.id,
                'update_url': update_url,
                })

        # застосовуємо піганацію до списку студентів
        # context = paginate(students, 10, self.request, context, var_name='students')

        # повертаємо оновлений словник із даними
        return context


def journal_list(request):
    today = datetime.datetime.today()
    month = date(today.year, today.month, 1)
    myear, mmonth = month.year, month.month
    number_of_days = monthrange(myear, mmonth)[1]
    queryset = Student.objects.all()
    gid = Group.objects.all()[:1]

    update_url = reverse('journal_list')
    students_list = []
    students = []
    for stud in queryset:
        try:
            journal = Journals.objects.get(student=stud, date=month)
        except Exception:
            journal = None

        days = []
        for day in range(1, number_of_days + 1):
            days.append({
                'day': day,
                'present': journal and getattr(journal, 'day%d' % day, False) or False,
                'date': date(myear, mmonth, day).strftime(
                    '%Y-%m-%d'),
            })

        # prepare metadata for current student
        students.append({
            'fullname': u'%s %s' % (stud.last_name, stud.first_name),
            'days': days,
            'id': stud.id,
            'update_url': update_url,
        })

    day_range = range(1, calendar.mdays[mmonth]+1)
    template = 'journal_new.html'

    context = {
        'day_range': day_range,
        'gid': gid,
        'students_list': students_list,
        'students': students,
    }

    return render(request, template, context)


def journal_change(requets, student_id, data, result):
    return HttpResponse('change the journal record')

