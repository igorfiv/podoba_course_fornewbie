# -*- coding: utf-8 -*-

from django.shortcuts import render
from django import forms
from django.core.mail import send_mail
from studentsdb.settings import ADMIN_EMAIL
from django.contrib import messages
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.views.generic import FormView



class ContactForm(forms.Form):

    def __init__(self, *args, **kwargs):

        super(ContactForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()

        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('contact_admin')

        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-2 control-label'
        self.helper.field_class = 'col-sm-10'

        self.helper.add_input(Submit('send_button', u'Надіслати'))


    from_email = forms.EmailField(
        label=u'Ваша email адреса'
    )

    subject = forms.CharField(
        label= u'Заголовок листа',
        max_length=128
    )

    message = forms.CharField(
        label=u'Текст повідомлення',
        max_length=2560,
        widget=forms.Textarea
    )


class ContactView(FormView):
    template_name = 'contact_form/contact_form.html'
    form_class = ContactForm
    success_url = '/email-sent'

    def form_valid(self, form):
        subject = form.cleaned_data['subject']
        message = form.cleaned_data['message']
        from_email = form.cleaned_data['from_email']

        send_mail(subject, message, from_email, ['admin@gmail.com'])

        return super(ContactView, self).form_valid(self)


def contact_admin(request):

    template = 'contact_admin/form.html'

    if request.method == 'POST':

        form = ContactForm

        form_new = form(data=request.POST)

        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            from_email = form.cleaned_data['from_email']

            try:
                send_mail(subject, message, from_email, [ADMIN_EMAIL])
            except Exception:
                message = u'Під час відправки листа виникла помилка'
            else:
                message = u'Повідомлення успішно відправлено'
                messages.success(request, message)
            return HttpResponseRedirect(reverse('contact_admin'))
    else:
        form = ContactForm()

    return render(request, template, {'form': form})


def contact(request):

    template = 'contact_form/contact_form.html'

    form = ContactForm

    if request.method == 'POST':

        form_new = form(data=request.POST)

        if form_new.is_valid():
            contact_name = request.POST.get('contact_name', '')
            contact_email = request.POST.get('contact_email', '')
            form_content = request.POST.get('content', '')


            try:
                send_mail(contact_name, form_content, contact_email, [ADMIN_EMAIL])
            except Exception:
                message = u'Під час відправки листа виникла помилка'
            else:
                message = u'Повідомлення успішно відправлено'
                messages.success(request, message)

            return HttpResponseRedirect(reverse('contact'))

    return render(request, template, {'form': form})

