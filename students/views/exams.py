# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render
from ..models import Exam

def exams_list(request):
    list = Exam.objects.all()

    template = 'exams.html'

    context = {
        'exam_list': list,
    }

    return render(request, template, context)
