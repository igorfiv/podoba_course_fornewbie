# -*- coding: utf-8 -*-

from django import forms
from django.urls import reverse
from students.models import Group, Student

from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
# import ipdb


class GroupCreateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'

    def clean(self):
        pass

    def __init__(self, *args, **kwargs):
        super(GroupCreateForm, self).__init__(*args, **kwargs)

        self.fields['leader'].queryset = Student.objects.filter(leader_profile__isnull=True)

        # ipdb.set_trace()

        self.helper = FormHelper(self)

        self.helper.form_action = reverse('group_add')
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form_horizontal'
        self.helper.html5_required = True
        self.helper.add_input(Submit('add_button', u'Додати', css_class="btn btn-primary"))
        self.helper.add_input(Submit('cancel_button', u'Скасувати', css_class="btn btn-link"))


class GroupUpdateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(GroupUpdateForm, self).__init__(*args, **kwargs)

        self.fields['leader'].queryset = Student.objects.filter(leader_profile__isnull=True)

        self.helper = FormHelper(self)

        self.helper.form_action = reverse('group_edit', kwargs={'pk': kwargs['instance'].id})
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form_horizontal'
        self.helper.html5_required = True
        self.helper.add_input(Submit('add_button', u'Зберегти', css_class="btn btn-primary"))
        self.helper.add_input(Submit('cancel_button', u'Скасувати', css_class="btn btn-link"))




# class GroupDeleteFrom(forms.ModelForm):
#     pass

