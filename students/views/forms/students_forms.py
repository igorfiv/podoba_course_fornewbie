# -*- coding: utf-8 -*-

# Forms for the students views

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.bootstrap import FormActions
from django.urls import reverse
from students.models import Student, Group
from bootstrap_datepicker.widgets import DatePicker


class StudentUpdateForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'middle_name', 'birthday', 'photo', 'ticket', 'students_group', 'notes']

    def clean(self):
        # import ipdb; ipdb.set_trace()
        cleaned_data = super(StudentUpdateForm, self).clean()

        if Group.objects.filter(leader=self.instance).exists():
            raise forms.ValidationError(u'Студент є старостою іншої групи. Спочатку необхідно змінити старосту',
                                        code='invalid')

        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(StudentUpdateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)

        self.helper.form_action = reverse('students_edit', kwargs={'pk': kwargs['instance'].id})
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form-horizontal'

        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-2 control-label'
        self.helper.field_class = 'col-sm-10'

        self.helper.add_input(Submit('add_button', u'Зберігти', css_class="btn btn-primary"))
        self.helper.add_input(Submit('cancel_button', u'Скасувати', css_class="btn btn-link"))


class StudentAddForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'

        birthday = forms.DateField(
            DatePicker(options={"format": "mm/dd/yyyy", "autoclose": True})
        )

    def __init__(self, *args, **kwargs):
        super(StudentAddForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)

        self.helper.form_action = reverse('students_add')
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form-horizontal'

        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-2 control-label'
        self.helper.field_class = 'col-sm-10'

        self.helper.layout[-1] = FormActions(
            Submit('add_button', u'Додати', css_class="btn btn-primary"),
            Submit('cancel_button', u'Скасувати', css_class="btn btn-link"),
        )

