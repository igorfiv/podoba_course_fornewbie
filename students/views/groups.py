# -*- coding: utf-8 -*-
from django import views
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from forms.groups_forms import *


class GroupCreateView(views.generic.CreateView):
    model = Group
    template_name = 'groups/groups_add.html'
    form_class = GroupCreateForm

    def get_success_url(self):
        return u'%s?status_message=Групу успішно додано!' % reverse('groups')

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel_button'):
            return HttpResponseRedirect(u'%s?status_message=Створення групи відмінено!' % reverse('groups'))
        else:
            return super(GroupCreateView, self).post(request, *args, **kwargs)


class GroupUpdateView(views.generic.UpdateView):
    model = Group
    template_name = 'groups/groups_edit.html'
    form_class = GroupUpdateForm

    def get_success_url(self):
        return u'%s?status_message=Групу успішно збережено!' % reverse('groups')

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel_button'):
            return HttpResponseRedirect(u'%s?status_message=Редагування групи відмінено!' % reverse('groups'))
        else:
            return super(GroupUpdateView, self).post(request, *args, **kwargs)


class GroupDeleteView(views.generic.DeleteView):
    model = Group
    template_name = 'groups/group_delete_confirm.html'

    def get_success_url(self):
        return u'%s?status_message=Групу успішно видалено!' % reverse('groups')


class GroupsView(views.generic.TemplateView):
    template_name = 'groups/groups_view.html'


def group_list(request):
    group_list = Group.objects.all()
    template = 'group.html'

    # Paginate groups
    paginator = Paginator(group_list, 10)
    page = request.GET.get('page')
    try:
        group_list = paginator.page(page)
    except PageNotAnInteger:
        group_list = paginator.page(1)
    except EmptyPage:
        group_list = paginator.page(paginator.num_pages)

    context = {
        'group': group_list,
    }

    return render(request, template, context)
