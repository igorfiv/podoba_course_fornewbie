# -*- coding: utf-8 -*-

from datetime import datetime

from django import views
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from forms.students_forms import *


class StudentsListView(views.generic.ListView):
    model = Student
    context_object_name = 'students'
    template_name = 'students/students-list.html'

    def get_context_data(self, **kwargs):
        context = super(StudentsListView, self).get_context_data(**kwargs)

        context['show_logo'] = False

        return context

    def get_queryset(self):
        qs = super(StudentsListView, self).get_queryset()

        return qs.order_by('last_name')


class StudentsUpdateView(views.generic.UpdateView):
    model = Student
    template_name = 'students/students_edit.html'
    form_class = StudentUpdateForm

    def get_success_url(self):
        return u'%s?status_message=Студента успішно збережено!' % reverse('home')

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel_button'):
            return HttpResponseRedirect(u'%s?status_message=Редагування студента відмінено!' % reverse('home'))
        else:
            return super(StudentsUpdateView, self).post(request, *args, **kwargs)


class StudentDeleteView(views.generic.DeleteView):
    model = Student
    template_name = 'students/students_delete_confirm.html'

    def get_success_url(self):
        return u'%s?status_message=Студента успішно видалено!' % reverse('home')


class StudentAddView(views.generic.CreateView):
    model = Student
    template_name = 'students/students_add.html'
    form_class = StudentAddForm

    def get_success_url(self):
        return u'%s?status_message=Студента успішно додано!' % reverse('home')

    def post(self, request, *args, **kwargs):
        if request.POST.get('cancel_button'):
            return HttpResponseRedirect(u'%s?status_message=Додавання студента відмінено!' % reverse('home'))
        else:
            return super(StudentAddView, self).post(request, *args, **kwargs)


def students_list(request, extra_context=None):
    students = Student.objects.all()

    order_by = request.GET.get('order_by', '')
    if order_by in ('last_name', 'first_name', 'ticket', 'pk'):
        students = students.order_by(order_by)
        default = '0'
        if request.GET.get('reverse', '') == '1':
            students = students.reverse()
    else:
        default = '1'
        students = students.order_by('last_name')

    # Paginate students
    # paginator = Paginator(students, 3)
    # page = request.GET.get('page')
    # try:
    #     students = paginator.page(page)
    # except PageNotAnInteger:
    #     students = paginator.page(1)
    # except EmptyPage:
    #     students = paginator.page(paginator.num_pages)

    template = 'students_list.html'
    page_template = 'students_load.html'
    ajax_flag = 0
    context = {
        'students': students,
        'default': default,
        'page_template': page_template,
        'ajax_flag': ajax_flag,
    }

    if request.is_ajax():
        template = page_template

    return render(request, template, context)


def students_add(request):
    groups = Group.objects.all().order_by('title')

    template = 'student_add.html'
    context = {'groups': groups,
               }

    if request.method == 'POST':
        if request.POST.get('add_button') is not None:

            errors = {}

            data = {'middle_name': request.POST.get('middle_name'),
                    'notes': request.POST.get('notes')}

            first_name = request.POST.get('first_name', '').strip()
            if not first_name:
                errors['first_name'] = u'Ім’я є обов’язковим'
            else:
                data['first_name'] = first_name

            last_name = request.POST.get('last_name', '').strip()
            if not last_name:
                errors['last_name'] = u'Прізвище є обов’язковим'
            else:
                data['last_name'] = last_name

            birthday = request.POST.get('birthday', '').strip()
            if not birthday:
                errors['birthday'] = u'Дата народження має бути заповнена'
            else:
                try:
                    datetime.strptime(birthday, '%Y-%m-%d')
                except Exception:
                    errors['birthday'] = u'Введіть коректний формат дати(напр. 1984-12-30)'
                else:
                    data['birthday'] = birthday

            ticket = request.POST.get('ticket', '').strip()
            if not ticket:
                errors['ticket'] = u'Номер білета має бути заповнений'
            else:
                data['ticket'] = ticket

            student_group = request.POST.get('student_group', '').strip(u'\u201d')
            if not student_group:
                errors['student_group'] = u'Виберіть группу'
            else:
                groups = Group.objects.filter(pk=student_group)
                if len(groups) != 1:
                    errors['students_group'] = u'Оберіть коректну групу'
                else:
                    data['students_group'] = groups[0]

            photo = request.FILES.get('photo')
            pos = 0
            if photo:
                pos = photo.name.rfind('.')
            if pos > 1:
                dl = photo.name[pos:]
                if (dl == '.jpg') or (dl == '.png'):
                    data['photo'] = photo
                else:
                    errors['photo'] = u'Неверный формат файла'

            if not errors:
                result = 'Студента додано'
                context.update({'result': result,
                                'data': data})
                student = Student(**data)
                student.save()
                full_name = data['first_name'] + ' ' + data['last_name']
                messages.success(request, u'Студента %s додано.' % full_name)
                return HttpResponseRedirect(u'%s?status_message=Студента %s успішно додано!' % (reverse('home'),
                                                                                                full_name))
            else:
                context.update({'errors': errors,
                                'data': data,
                                'std_gr': student_group})
                return render(request, template, context)

        elif request.POST.get('cancel_button') is not None:
            return render(request, template, context)
    else:
        return render(request, template, context)


def students_edit(request, sid):
    return HttpResponse('Edit student  %s' % sid)


def students_delete(request, sid):
    return HttpResponse('Delete student %s' % sid)
