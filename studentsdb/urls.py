"""studentsdb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from students.views.groups import *
from students.views.students import *
from students.views.journals import *
from students.views.exams import *
from students.views.contact_admin import *
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    # Students urls
    url(r'^$', students_list, name='home'),
    url(r'^students-list/$', StudentsListView.as_view()),
    url(r'^students/add/$', StudentAddView.as_view(), name='students_add'),
    url(r'^students/(?P<pk>\d+)/delete/$', StudentDeleteView.as_view(), name='students_delete'),
    url(r'^students/(?P<pk>\d+)/update/$', StudentsUpdateView.as_view(), name='students_edit'),

    # Group urls
    url(r'^groups/$', group_list, name='groups'),
    url(r'^groups1/$', GroupsView.as_view(),name='groups_'),
    url(r'^groups/add/$', GroupCreateView.as_view(), name='group_add'),
    url(r'^groups/(?P<pk>\d+)/edit/$', GroupUpdateView.as_view(), name='group_edit'),
    url(r'^groups/(?P<pk>\d+)/delete/$', GroupDeleteView.as_view(), name='group_delete'),
    url(r'^admin/', include(admin.site.urls)),

    # Exams

    url(r'^exams/$', exams_list, name='exams_list'),

    # Contact admin url

    url(r'^contact-admin/$', contact_admin, name='contact_admin'),
    url(r'^contact-class/$', ContactView.as_view()),
    url(r'^contact/', contact, name='contact'),

    # journal
    url(r'^journal_/$', journal_list, name='journal_list_'),
    url(r'^journal/$', JournalsView.as_view(), name='journal_list'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)